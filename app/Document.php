<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Document extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'documents';

    protected $fillable=[
    	'titulo',
    	'autor',
    	'descripcion',
    	'file',
    ];
}