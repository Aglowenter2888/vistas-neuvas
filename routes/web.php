<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/capturaExp', function () {
    return view('expedientes.create');
});

Route::get('/header', function () {
    return view('layouts.header');
});

Route::get('/perfil', function () {
    return view('users.perfil');
});
Route::get('/tutoria', function () {
    return view('users.tutoria');
});

Route::get('/Gestioncademica', function () {
    return view('users.Gestioncademica');
});
Route::get('/Productos_desarrollados', function () {
    return view('users.Productos_desarrollados');
});
Route::get('/capacitacion', function () {
    return view('users.capacitacion');
});
Route::get('/Eventos', function () {
    return view('users.Eventos');
});
Route::get('/difuncion', function () {
    return view('Formatos.difuncion');
});
Route::get('/articulos', function () {
    return view('Formatos.articulos');
});
Route::get('/assesorias', function () {
    return view('Formatos.assesorias');
});
Route::get('/libros', function () {
    return view('Formatos.libros');
});
Route::get('/informe', function () {
    return view('Formatos.informe');
});
Route::get('/arbitrado', function () {
    return view('Formatos.arbitrado');
});
Route::get('/didactico', function () {
    return view('Formatos.didactico');
});
Route::get('/investigacion', function () {
    return view('Formatos.investigacion');
});

Route::group(['middleware' => ['role:PTC']], function () {
    Route::get('/articulo', function () {
    return view('users.articulo');
});
});


Route::get('/editarperfil', function () {
    return view('users.editarperfil');
});

Route::get('/libro', function () {
    return view('users.libro');
});

Route::get('/inforTec', function () {
    return view('users.informe_tec');
});


//PDF capturaExp
Route::get('/capturaExp', 'PdfController@pdfForm');
Route::post('pdf_download', 'PdfController@pdfDownload');

//PDF GestionAcademica
Route::get('/Gestioncademica', 'PdfController@pdfFormGestion');
Route::post('pdf_download_GestionA', 'PdfController@pdfDownload_GestionA');

//PDF Producto_desarrollado
Route::get('/Productos_desarrollados', 'PdfController@pdfFormProductos');
Route::post('pdf_download_productosD', 'PdfController@pdfDownload_Productos');


//Documentos
Route::get('/files/create','DocumentosController@create');
Route::post('/files','DocumentosController@store');

Route::get('/files','DocumentosController@index');
Route::get('/files/{id}','DocumentosController@show');
Route::get('file/download/{file}','DocumentosController@download');

//Busqueda
Route::get('/search','DocumentosController@search');
Route::get('/fechaActual','DocumentosController@fechaActual');
Route::get('/pasado','DocumentosController@primerAño');
Route::get('/antiguo','DocumentosController@tercerAño');

Route::resource('archivos','DocumentosController');
Route::resource('usuario','UserController');




Auth::routes();

